'use strict'
const moment = require('moment')
const Class = require('../models/Class')

const controller = {}

/**
 * Get classes for admin view. If upcoming returns the coming 14 days, if false returns the past 14 days.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
controller.getClasses = async (req, res) => {
  const classesToReturn = []
  if (req.query.upcoming === 'true') {
    for (let i = 0; i < 15; i++) {
      const date = moment().add(i, 'days')
      // start today
      const start = moment(date).startOf('day')
      // end today
      const end = moment(date).endOf('day')

      const classes = await Class.find({
        startTime: {
          $gte: start,
          $lte: end
        }
      })
      const obj = {
        day: date,
        classes: classes
      }
      console.log(obj)
      if (obj.classes.length > 0) {
        classesToReturn.push(obj)
      }
    }
  } else if (req.query.past === 'true') {
    for (let i = 1; i < 15; i++) {
      const date = moment().subtract(i, 'days')
      // start today
      const start = moment(date).startOf('day')
      // end today
      const end = moment(date).endOf('day')

      const classes = await Class.find({
        startTime: {
          $gte: start,
          $lte: end
        }
      })

      const obj = {
        day: date,
        classes: classes
      }
      if (obj.classes.length > 0) {
        classesToReturn.push(obj)
      }
    }
  } else {
    for (let i = 0; i < 7; i++) {
      const date = moment().add(i, 'days')
      // start today
      const start = moment(date).startOf('day')
      // end today
      const end = moment(date).endOf('day')

      const classes = await Class.find({
        startTime: {
          $gte: start,
          $lte: end
        }
      })

      const obj = {
        day: date,
        classes: classes
      }
      classesToReturn.push(obj)
    }
  }
  res.status(200).json(classesToReturn)
}

/**
 * Adds class to Schedule.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
controller.addClass = async (req, res) => {
  try {
    const newClass = {
      class: req.body.class,
      startTime: req.body.startTime,
      duration: req.body.duration,
      venue: req.body.venue,
      instructor: req.body.instructor,
      status: req.body.status,
      maxParticipants: req.body.maxParticipants
    }

    Class.create(newClass, async (err, doc) => {
      if (err) throw err
      if (doc) {
        res.status(201).json({ message: 'Class created', class: doc })
      }
    })
  } catch (error) {
    res.sendError(error)
  }
}

/**
 * Removes class from Schedule.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
controller.removeClass = async (req, res) => {
  try {
    Class.deleteOne({ _id: req.params.id }, (error, doc) => {
      if (error) throw new Error(error)
      if (doc.deletedCount === 0) res.status(400).json({ error: 'No class was deleted' })
      else {
        res.status(200).json({ message: 'Successfully deleted', class: doc })
      }
    })
  } catch (error) {
    res.sendError({ error: error })
  }
}

/**
 * Adds a participant to a class.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
controller.addParticipant = async (req, res) => {
  try {
    const newParticipant = {
      userID: req.body.userID,
      displayName: req.body.userDisplayName,
      paymentStatus: req.body.paymentStatus
    }
    const classToUpdate = await Class.findOne({ _id: req.body.classID })
    const signedUpList = classToUpdate.participants
    const maxParticipants = classToUpdate.maxParticipants

    if (signedUpList.length >= maxParticipants) {
      res.status(400).json({ message: 'Class is full unfortunately' })
    }
    if (signedUpList.some(user => user.userID === req.body.userID)) {
      res.status(405).json({ message: 'You are already booked to this class' })
    } else {
      classToUpdate.participants.push(newParticipant)
      await classToUpdate.save()
      res.status(201).json({ message: 'User registered to class' })
    }
  } catch (error) {
    res.sendError(error)
  }
}

/**
 * Removes particioant from a class.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
controller.removeParticipant = async (req, res) => {
  try {
    const participantToRemove = {
      userID: req.body.userID
    }
    const doc = await Class.updateOne({ _id: req.body.classID }, { $pull: { participants: participantToRemove } })
    if (!doc) {
      throw new Error('Class not found')
    }
    if (doc.nModified === 1) res.status(200).json({ message: `User ${req.body.userID} removed from the class` })
    if (doc.nModified === 0) res.status(400).json({ error: `User ${req.body.userID} could not be found on that class.` })
  } catch (error) {
    res.sendError({ error: error })
  }
}

/**
 * Change status to confirmed in classes.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
controller.changeStatus = async (req, res) => {
  try {
    Class.updateOne({ _id: req.params.id }, { status: 'confirmed' }, async (err, doc) => {
      if (err) throw err
      if (doc.nModified === 1) {
        res.status(200).json({ message: `Class ${req.params.id} has now status confirmed` })
      }
      if (doc.nModified === 0) {
        res.status(405).json({ error: `Class ${req.params.id} has had no status change` })
      }
      if (!doc) {
        res.status(404).json({ error: 'Class not found' })
      }
    })
  } catch (error) {
    res.sendError(error)
  }
}

module.exports = controller

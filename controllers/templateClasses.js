'use strict'
const TemplateClass = require('../models/TemplateClass')

const controller = {}

/**
 * Get template classes.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
controller.getClasses = async (req, res) => {
  const classes = await TemplateClass.find()
  res.status(200).json({ message: 'Classes found', classes: classes })
}

/**
 * Add a class to template class week.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
controller.addClass = async (req, res) => {
  try {
    const newClass = {
      class: req.body.class,
      day: req.body.day,
      startTime: req.body.startTime,
      duration: req.body.duration,
      venue: req.body.venue,
      instructor: req.body.instructor,
      maxParticipants: req.body.maxParticipants
    }

    TemplateClass.create(newClass, async (err, doc) => {
      if (err) throw err
      if (doc) {
        res.status(201).json({ message: 'Class created', class: doc })
      }
    })
  } catch (error) {
    res.sendError(error)
  }
}

/**
 * Delete a class from template class week.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
controller.deleteClass = async (req, res) => {
  try {
    TemplateClass.deleteOne({ _id: req.params.id }, (err, doc) => {
      if (err) throw new Error(err)
      if (doc.deletedCount === 1) res.status(200).json({ message: 'Successfully deleted', class: doc })
      if (doc.deletedCount === 0) res.status(400).json({ error: 'No class deleted', class: doc })
    })
  } catch (error) {
    res.sendError(error)
  }
}

module.exports = controller

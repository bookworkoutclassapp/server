const express = require('express')
const router = express.Router()

const controller = require('../controllers/classes')

router.get('/', controller.getClasses)
router.post('/', controller.addClass)
router.delete('/:id', controller.removeClass)
router.post('/:id', controller.addParticipant)
router.delete('/:id/cancel', controller.removeParticipant)
router.put('/:id', controller.changeStatus)

module.exports = router

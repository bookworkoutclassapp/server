const express = require('express')
const router = express.Router()

const controller = require('../controllers/templateClasses')

router.get('/', controller.getClasses)
router.post('/', controller.addClass)
router.delete('/:id', controller.deleteClass)

module.exports = router

const express = require('express')
const passport = require('passport')
const User = require('../models/User')
const bcrypt = require('bcryptjs')
const router = express.Router()

/**
 * Endpoint for Line login/register.
 */
router.get('/line', passport.authenticate('line', { scope: ['P'] }))

/**
 * Endpoint for Line callback.
 */
router.get('/line-callback',
  passport.authenticate('line', { failureRedirect: '/sign-in' }),
  (req, res) => {
    res.redirect(`${process.env.CLIENT_URL}/signing-in?userID=${req.user.lineID}&&displayName=${req.user.displayName}&&isAdmin=${req.user.isAdmin}`)
  }
)

/**
 * Endpoint for signing in a user.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
router.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) throw err
    if (!user) res.status(401).json({ message: 'No matching user' })
    else {
      req.logIn(user, (err) => {
        if (err) throw err
        res.status(200).json({ message: 'Successfully authenticated', user: { userID: user._id, displayName: user.displayName, isAdmin: user.isAdmin } })
      })
    }
  })(req, res, next)
})

/**
 * Endpoint for registering a user.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 */
router.post('/register', (req, res) => {
  User.findOne({ email: req.body.email }, async (err, doc) => {
    if (err) throw err
    if (doc) {
      res.status(400).json({ message: 'User already exists' })
    }
    if (!doc) {
      const hashedPassword = await bcrypt.hash(req.body.password, 10)
      const newUser = new User({
        email: req.body.email,
        password: hashedPassword,
        displayName: req.body.displayName,
        instagram: req.body.instagram
      })
      const user = await newUser.save()
      res.status(201).json({ message: 'User saved', user: user })
    }
  })
})

module.exports = router

'use strict'
const moment = require('moment')
const TemplateClass = require('../models/TemplateClass')
const Class = require('../models/Class')

const config = {}

/**
 * Creates new classes in Schedule from Weekly Template.
 */
config.copyTemplateSchedule = async () => {
  console.log(`Copied classes from template to shedule ${moment().format('YYYY-MM-DD HH:mm:ss')}`)
  const dateInTwoWeeks = moment().add(15, 'days').format('YYYY-MM-DD')
  const weekDayInTwoWeeks = moment(dateInTwoWeeks).format('dddd')
  console.log(dateInTwoWeeks)

  const response = await fetchTemplateClasses(weekDayInTwoWeeks)
  for (let i = 0; i < response.length; i++) {
    const newClass = new Class({
      day: dateInTwoWeeks,
      class: response[i].class,
      startTime: response[i].startTime,
      duration: response[i].duration,
      venue: response[i].venue,
      instructor: response[i].instructor,
      status: 'unconfirmed',
      maxParticipants: response[i].maxParticipants
    })
    newClass.save(function (err) {
      if (err) console.log(err)
    })
  }
}

/**
 * Get template classes from DB on the given day.
 *
 * @param {string} weekday  Weekday.
 * @returns {Array} Return array with classes for the requested weekday.
 */
const fetchTemplateClasses = async (weekday) => {
  try {
    const response = await TemplateClass.find({ day: weekday })
    return response
  } catch (err) {
    console.log(err)
  }
}

module.exports = config

'use strict'

const LineStrategy = require('passport-line').Strategy
const LocalStrategy = require('passport-local').Strategy
const User = require('../models/User')
const bcrypt = require('bcryptjs')

// Code from: http://www.passportjs.org/packages/passport-gitlab2/

/**
 * Gitlab Strategy for passsport.
 *
 * @param {object} passport Importing the passport module.
 */
module.exports = function (passport) {
  passport.use(new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
    User.findOne({ email: email }, (err, user) => {
      if (err) throw err
      if (!user) return done(null, false)
      bcrypt.compare(password, user.password, (err, result) => {
        if (err) throw err
        if (result === true) {
          return done(null, user)
        } else {
          return done(null, false)
        }
      })
    })
  }))

  // Line strategy for passport.
  passport.use(new LineStrategy({
    channelID: process.env.LINE_CHANEL_ID,
    channelSecret: process.env.LINE_CHANNEL_SECRET,
    callbackURL: process.env.LINE_CALLBACK
  },
  async (accessToken, refreshToken, profile, done) => {
    try {
      const newUser = {
        lineID: profile.id,
        displayName: profile.displayName,
        lineAccessToken: accessToken,
        isAdmin: false
      }
      let user = await User.findOne({ lineID: profile.id })

      if (user) {
        done(null, user)
      } else {
        user = await User.create(newUser)
        done(null, user)
      }
    } catch (err) {
      console.error(err)
    }
  }
  ))

  passport.serializeUser((user, done) => {
    done(null, user.id)
  })

  passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => done(err, user))
  })
}

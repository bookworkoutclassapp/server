'use strict'

const mongoose = require('mongoose')

const TemplateClassSchema = new mongoose.Schema({
  day: {
    type: String,
    required: true,
    enum: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
  },
  class: {
    type: String,
    required: true,
    enum: ['Handstand', 'Calisthenics', 'Handstand Push Ups', 'Planche']
  },
  startTime: {
    type: String,
    required: true
  },
  duration: {
    type: Number,
    default: 60
  },
  venue: {
    type: String,
    default: 'Siam Strength'
  },
  instructor: {
    type: String,
    default: 'Hakim Azahar'
  },
  maxParticipants: {
    type: Number,
    default: 10
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('TemplateClass', TemplateClassSchema)

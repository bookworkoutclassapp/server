'use strict'

const mongoose = require('mongoose')

const ClassSchema = new mongoose.Schema({
  class: {
    type: String,
    required: true
  },
  startTime: {
    type: Date,
    required: true
  },
  duration: {
    type: Number,
    default: 60
  },
  venue: {
    type: String,
    default: 'Siam Strength'
  },
  instructor: {
    type: String,
    default: 'Hakim Azahar'
  },
  status: {
    type: String,
    default: 'unconfirmed',
    enum: ['unconfirmed', 'confirmed']
  },
  maxParticipants: {
    type: Number,
    default: 10
  },
  participants: {
    type: Array,
    default: []
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('Class', ClassSchema)

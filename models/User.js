'use strict'

const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    minlength: [5, 'Provide a valid email-adress'],
    validate: [/^[a-zA-Z0-9]+([-'@.]+[sa-zA-Z0-9]+)*$/, 'Provide a valid email']
  },
  password: {
    type: String,
    minlength: [6, 'Password must have minimum 6 characters']
  },
  lineID: {
    type: String
  },
  displayName: {
    type: String,
    required: true,
    minlength: [1, 'Provide minimum 1 characters']
  },
  lineAccessToken: {
    type: String
  },
  instagram: {
    type: String
  },
  isAdmin: {
    type: Boolean,
    required: true,
    default: false,
    enum: [false, true]
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('User', UserSchema)

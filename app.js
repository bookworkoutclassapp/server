'use strict'

// Import
const createError = require('http-errors')
const express = require('express')
const session = require('express-session')
const path = require('path')
const connectDB = require('./config/mongoose')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const passport = require('passport')
const authRouter = require('./routes/auth')
const classesRouter = require('./routes/classes')
const templateClassesRouter = require('./routes/templateClasses')
const cron = require('node-cron')
const config = require('./config/cronJob')
require('dotenv').config()
// ----------------- End of Import-----------------
const app = express()
connectDB()

// Middleware
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors({
  origin: process.env.CLIENT_URL,
  credentials: true
}))
app.use(
  session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
  })
)
app.use(cookieParser('keyboard cat'))
app.use(express.static(path.join(__dirname, 'public')))
app.use(passport.initialize())
app.use(passport.session())
require('./config/passport')(passport)
// ----------------- End of Middleware-----------------

// Routes
app.use('/auth', authRouter)
app.use('/classes', classesRouter)
app.use('/template-class', templateClassesRouter)
// ----------------- End of Routes-----------------

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

// Cron-job runs 6 am every day to clone template into schedule.
cron.schedule('0 6 * * *', () => {
  config.copyTemplateSchedule()
})

module.exports = app

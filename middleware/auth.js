'use strict'

module.exports = {

  /**
   * Ensures Auth.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {object} next - Express next object.
   * @returns {object} - next.
   */
  ensureAuth: function (req, res, next) {
    if (req.isAuthenticated()) {
      return next()
    } else {
      res.redirect('/')
    }
  },

  /**
   * Ensures Guests.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {object} next - Express next object.
   * @returns {object} - next.
   */
  ensureGuest: function (req, res, next) {
    if (req.isAuthenticated()) {
      res.redirect('http://localhost:3000/book-class')
    } else {
      return next()
    }
  }
}
